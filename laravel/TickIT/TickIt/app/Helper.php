<?php

namespace App;
use DateTime;
class Helper {
    /**
    * [dateAUtoUS description]
    * converts a au date to a us date
    * @param  string $date - date format day/month/year = d/m/Y
    * @return string       - returns US date in a string
    */
    public static function dateAUtoUS ($date ){
        $dateOBJ = DateTime::createFromFormat('d/m/Y',$date);
        return $dateOBJ->format('m/d/Y');
    }
    /**
    * [dateUStoAU description]
    * converts a us date to a au date
    * @param  string $date date format month/day/year = m/d/Y
    * @return string    - returns AU date in a string
    */
    public static function dateUStoAU ($date){
        $dateOBJ = DateTime::createFromFormat('m/d/Y',$date);
        return $dateOBJ->format('d/m/Y');
    }

    /**
    * [dateUStoAU description]
    * converts a us date to a au date
    * @param  string $date date format day/month/year = m/d/Y
    * @return Carbon    - returns carbon object sutable for use with MYSQL
    */
    public static function dateAUtoMYSQL ($date){
        return DateTime::createFromFormat('d/m/Y',$date);
    }
}

?>
