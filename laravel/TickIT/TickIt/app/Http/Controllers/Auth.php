<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Validator;
use Auth as LAuth;
// use Illuminate\Http\Request;
use JWTAuth;

class Auth extends Controller{
/**
 * this function logs the user in and creates a new session
 * @param  Request $request - email & password
 * @return JSON           sucess/fail & errors if any
 */
    public function login (Request $request){
        $post = $request->all();
        $credentials = $request->only('email', 'password');
        $ok =  Validator::make($post, [
            'email' => 'required|email|max:255|min:1',
            'password' => 'required|string|max:255|min:1',
        ]);
        if (!$ok->fails()){ //if passes
            $user = User::where('email','=', $post['email'])->first();
            $token = JWTAuth::attempt($credentials);
            // dd($token);
            if ($token){
                // $token = JWTAuth::fromUser($user);
                return response()->json(['status' => true, 'body' => [
                    'id'=>$user->id,
                    'token'=> $token,
                    'userName' => $user->userName,
                    'name'=> $user->name,
                    'users' => User::all(),
                ]]);
            }else{
                return response()->json(['status' => false, 'body' => [
                    'email' => ["Your details don't match our records."]
                ]]);
            }
        }else{//failed validator
            return response()->json(['status' => false, 'body' => $ok->errors()->messages()]);
        }
    }


    /**
     * this function checks to see if the user is loged in - use only for debugging jwt
     * @param  Request $request - email & password
     * @return JSON          sucess/fail & errors if any
     */
    public function check(Request $request){
        try {
            JWTAuth::parseToken();
            $user = JWTAuth::parseToken()->authenticate();
            return $user;
        }catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['status' => false, 'body' => [
                'JWT-E' => "Your Not Authenticated."
            ]]);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['status' => false, 'body' => [
                'JWT-I' => "Your Not Authenticated."
            ]]);
        }catch(\Tymon\JWTAuth\Exceptions\JWTException $e){
            return response()->json(['status' => false, 'body' => [
                'JWT-II' => "Your Not Authenticated."
            ]]);
        }
    }

    /**
     * this function logs the user out and returns a json responce
     * note this function is no longer required - to kill a session just remove the token from local storage
     * @return JSON sucess only
     */
    public function logout(){
        // if (LAuth::check()){
        //     LAuth::logout();
        // }
        // return response()->json(['status' => true, 'body' => []]);
    }
}
