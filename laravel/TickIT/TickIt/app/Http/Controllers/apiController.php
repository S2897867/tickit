<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ticket;
use Validator;
use Illuminate\Validation\Rule;

class apiController extends Controller{


     public $errors =[ // this array is used to store all error varables
         'DBMS' => "We had a problem creating a new user.",
         'BAD-REQ' => "The request data was bad."
     ];
     public $pageSize = 4;

     /**
      * This function creates a new user or returns any validation errors
      * @param  Request $request [description]
      * @return [type]           [description]
      */
    public function createUser (Request $request){
        $put = $request->all();
        $ok = User::isOk($put);//check input is ok for creating user
        if (!is_array ($ok)){
            $user = User::newUser($put);//create user
            if ($user){//check user
                return response()->json(['status' => true, 'body' =>[]]);
            }else{//return on fail to create
                return response()->json(['status' => false, 'body' => [
                    'DBMS' => $this->errors['DBMS'],
                    ]]);
            }
        }else{//return on fail to validate with errors
            return response()->json(['status' => false, 'body' => $ok]);
        }
    }

    public function createTicket (Request $request){
        $put = $request->all();
        $ok = Ticket::isOk($put);
        if (!is_array ($ok)){
            $ticket = Ticket::createTicket($put);
            if ($ticket){//check ticket
                return response()->json(['status' => true, 'body' =>[]]);
            }else{//return on fail to create
                return response()->json(['status' => false, 'body' => [
                    'DBMS' => $this->errors['DBMS']
                    ]]);
            }
        }else{//return fail to validate
            return response()->json(['status' => false, 'body' => $ok]);
        }
    }
    /**
     * this function gets all all the curent users and returns them as an araray.
     * @return JSON an array of users
     */
    public function getAllUsers (){
        $users = User::all();
        return response()->json(['status' => true, 'body' => $users]);
    }

/**
 * this function gets a list of tickets - paginated
 * @param  Int $page     the page number for pagination
 * @param  Bool $resolved a bool that is used to tell the search to include soft deledted records
 * @return JSON          a list of tickets
 */
    public function getAllTickets ($page,$resolved){
        $data =[//check input date to see if valid
            'page' => $page,
            'resolved' => $resolved,
        ];
        $ok =  Validator::make($data, [
            'page' => 'required|integer|min:1',
            'resolved' => ['required', Rule::in(['true', 'false']),],
        ]);
        if (!$ok->fails()){//if valid than build search
            $page-=1;
            // dd($resolved);
            if ($resolved == "true"){//get all tickets and ones that have been resolved
                $tickets = Ticket::withTrashed()->where([
                    ['id','>','0' ]
                ]);
            }else{//get all unresolved tickets
                $tickets = Ticket::where([
                    ['id','>','0' ]
                ]);
            }//run search and return tickets
            $tickets = $tickets
              ->orderBy('priority', 'asc')
              ->orderBy('due_date', 'asc')
              ->skip($this->pageSize*$page)
              ->take($this->pageSize)->get();
            return response()->json(['status' => true, 'body' => $tickets]);
        }else{
            return response()->json(['status' => false, 'body' => [
                'BAD-REQ' => $this->errors['BAD-REQ'],
                ]]);
        }
    }


    /**
     * this function gets a list of tickets - paginated
     * @param  Request $request - the request var from post + the user object added by Middleware
     * @param  Int $page     the page number for pagination
     * @param  Bool $resolved a bool that is used to tell the search to include soft deledted records
     * @return JSON          a list of tickets
     */
    public function getMyTickets (Request $request,$page,$resolved){
        $userId = $request->user['id'];//get the user id from the user object stored in midleware
        $data =[//check input date to see if valid
            'page' => $page,
            'resolved' => $resolved,
        ];
        $ok =  Validator::make($data, [
            'page' => 'required|integer|min:1',
            'resolved' => ['required', Rule::in(['true', 'false']),],
        ]);
        if (!$ok->fails()){//if valid than build search
            $page-=1;
            if ($resolved == "true"){//get all tickets and ones that have been resolved
                $tickets = Ticket::withTrashed()->where([
                    ['id','>','0' ],
                    ['assigned_to_id','=',$userId],
                ]);
            }else{//get all unresolved tickets
                $tickets = Ticket::where([
                    ['id','>','0' ],
                    ['assigned_to_id','=',$userId]
                ]);
            }//run search and return tickets
            $tickets = $tickets
              ->orderBy('priority', 'asc')
              ->orderBy('due_date', 'asc')
              ->skip($this->pageSize*$page)
              ->take($this->pageSize)->get();
            return response()->json(['status' => true, 'body' => $tickets]);
        }else{
            return response()->json(['status' => false, 'body' => [
                'BAD-REQ' => $this->errors['BAD-REQ'],
                ]]);
        }
    }

    /**
     * this function updates a ticket - it can be used for both edit and for marking a tickets as resolved
     * @param  Request $request - the request var from post + the user object added by Middleware
     * @return JSON           sucess/fail
     */
    public function updateTicket (Request $request){
        $patch  = $request->all();
        $ok = Ticket::isOkToPatch($patch);//check input data
        if (!is_array($ok)){ //if  ok to be patched
            $ticket = Ticket::updateTicket($patch);
            //return sucess
            return response()->json(['status' => true, 'body' => []]);
        }else{//return with errors
            return response()->json(['status' => false, 'body' => $ok]);
        }
    }
}
