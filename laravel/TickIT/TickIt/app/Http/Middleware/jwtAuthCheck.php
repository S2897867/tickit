<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class jwtAuthCheck{
    /**
     * This middleware is designed to reject requests on the API route that don't have a token as part of their post body
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        try {//check token
            JWTAuth::parseToken();
            $user = JWTAuth::parseToken()->authenticate();
            $request->user = $user;
        }catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {//if toekn expired
            return response()->json(['status' => false, 'body' => [
                'JWT-E' => "Your Not Authenticated."
            ]]);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {//if token but not complete
            return response()->json(['status' => false, 'body' => [
                'JWT-I' => "Your Not Authenticated."
            ]]);
        }catch(\Tymon\JWTAuth\Exceptions\JWTException $e){//if token not part of the body
            return response()->json(['status' => false, 'body' => [
                'JWT-II' => "Your Not Authenticated."
            ]]);
        }
        return $next($request);
    }
}
