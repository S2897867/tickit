<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use App\Helper;
use Validator;

class Ticket extends Model{
    use SoftDeletes;

    protected $table = 'tickets';
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'due_date'];

    public static function isOk ($ticket){
        $ok =  Validator::make($ticket, [
            'title' => 'required|string|max:100|min:3',
            'description' => 'required|string|max:1000|min:10',
            'priority' => ['required', Rule::in(['1', '2','3']),],
            'due_date' => 'required|date_format:"d/m/Y"',
            'created_by_id' => 'required|integer',
            'assigned_to_id' => 'required|integer',
        ]);
        if (!$ok->fails()){
            return true;
        }else{
            return $ok->errors()->messages();
        }
    }
    public static function isOkToPatch ($ticket){
        $ok =  Validator::make($ticket, [
            'title' => 'required|string|max:100|min:3',
            'description' => 'required|string|max:1000|min:10',
            'priority' => ['required', Rule::in(['1', '2','3']),],
            'due_date' => 'required|date_format:"d/m/Y"',
            'created_by_id' => 'required|integer',
            'assigned_to_id' => 'required|integer',
            'resolved_at' => 'date_format:"d/m/Y"',
        ]);
        if (!$ok->fails()){
            return true;
        }else{
            return $ok->errors()->messages();
        }
    }


    /**
     * this function creates a new ticket from input data.
     * @param  array $data a key array containt the new ticket data
     * @return Ticket       the new ticket obect is returend
     */
    public static function createTicket ($data){
        $ticket = new Ticket();
        $ticket->title = $data['title'];
        $ticket->priority = $data['priority'];
        $ticket->description = $data['description'];
        $ticket->due_date = Helper::dateAUtoMYSQL($data['due_date']);
        $ticket->created_by_id = $data['created_by_id'];
        $ticket->assigned_to_id = $data['assigned_to_id'];
        $ticket->save();
        return $ticket;
    }
    /**
     * this function is used to update a ticket
     * @param  array $data a key array containt the new ticket data
     * @return Ticket       the new ticket obect is returend or null on fail
     */
    public static function updateTicket ($data){
        $ticket = Ticket::withTrashed()->where('id',$data['id'])->get();
        if (count($ticket) > 0){
            $ticket = $ticket[0];
            $ticket->title = $data['title'];
            $ticket->priority = $data['priority'];
            $ticket->description = $data['description'];
            $ticket->assigned_to_id = $data['assigned_to_id'];
            $ticket->due_date = Helper::dateAUtoMYSQL($data['due_date']);
            //in the case the ticket is resolved than soft delete
            if (array_key_exists('resolved_at',$data) && !is_null($data['resolved_at'])){
                $ticket->deleted_at = Helper::dateAUtoMYSQL($data['resolved_at']);
            }else{// in the case it is not set the soft delet to null
                $ticket->deleted_at = null;
            }
            $ticket->save();
        }else{
            $ticket = null;
        }
        return $ticket;
    }
    /**
     * this function is part of the orm - ticket is the responsability of user
     * @return User the user that has been assigned to the ticket
     */
    public function responsableUser (){
        return $this->hasOne('App\User', 'id', 'assigned_to_id')->get();
    }

    /**
     * this function is part of the orm - ticket was created by user
     * @return User the user that created ticket
     */
    public function lodgedBy (){
        return $this->hasOne('App\User', 'id', 'created_by_id')->get();
    }

    /**
     * this function will resolve a ticket - this will soft delete it
     * @param  Ticket $ticket the ticket to resolve
     * @return Ticket         the modifyed ticket is returend
     */
    public static function resolvedticket ($ticket){
        $ticket->delete();
        return $ticket;
    }

    /**
     * this function will resolve a ticket - this will reverse the soft delete
     * @param  Ticket $ticket the ticket to reOpen
     * @return Ticket         the modifyed ticket is returend
     */
    public static function reOpenticket ($ticket){
        $ticket->restore();
        return $ticket;
    }
}
