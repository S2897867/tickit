<?php

namespace App;

use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Validator;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'userName',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


     /**
      * this function validates a user - this works with request or user model
      * @param  array||User  $data a key array/User that contains the user to validate
      * @return bool on pass || errors on fail
      */
     public static function isOk ($user){
         $ok =  Validator::make($user, [
             'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
             'userName' => 'required|string|max:100|min:3',
             'name' => 'required|string|max:100|min:3',
             'password' => 'required|string|max:255|min:4|confirmed',
         ]);
         if (!$ok->fails()){
             return true;
         }else{
             return $ok->errors()->messages();
         }
     }
     /**
      * this function creates a new user from input data.
      * @param  array $data a key array containt the new users data
      * @return User       the new user obect is returend
      */
    public static function newUser($data){
        $user = new User();
        $user->name = $data["name"];
        $user->userName = $data["userName"];
        $user->email = $data["email"];
        $user->password = Hash::make($data["password"]);
        $user->save();
        return $user;
    }

    /**
     * this function finds all the tickets that have been assiged to the user
     * @return Tickets an array ot tickets
     */
    public function assignedTickets (){
        return $this->hasMany('App\Ticket', 'assigned_to_id', 'id');
    }

    /**
     * this function finds all the tickets that have been created by the user
     * @return Tickets an array ot tickets
     */
    public function createdTickets (){
        return $this->hasMany('App\Ticket', 'created_by_id', 'id');
    }

    /**
     * JWT template
     */
     /**
      * Get the identifier that will be stored in the subject claim of the JWT.
      * @return mixed
      */
     public function getJWTIdentifier(){
         return $this->getKey();
     }

     /**
      * Return a key value array, containing any custom claims to be added to the JWT.
      *
      * @return array
      */
     public function getJWTCustomClaims() {
         return [];
     }


}
