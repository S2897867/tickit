<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Ticket;

class DatabaseSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        //insert two new users
        $user = User::newUser([
            'name' => "Zach",
            'userName' => "JoueBie",
            'email' => "Zcr2244@live.com",
            'password' => "1234",
        ]);
        $user = User::newUser([
            'name' => "User2",
            'userName' => "User2 UN",
            'email' => "Zcr2244@gmail.com",
            'password' => "1234",
        ]);
        //create demo tickets
        $ticket = Ticket::createTicket([
            'title' => "Ticket 1",
            'priority' => "M",
            'description' => "Short description for number one.",
            'due_date' => "27/08/2018",
            'created_by_id' => 1,
            'assigned_to_id' => 2,
        ]);
        $ticket = Ticket::createTicket([
            'title' => "Ticket 2",
            'priority' => "L",
            'description' => "Short description for number two.",
            'due_date' => "27/08/2018",
            'created_by_id' => 1,
            'assigned_to_id' => 2,
        ]);
        $ticket = Ticket::createTicket([
            'title' => "Ticket 3",
            'priority' => "H",
            'description' => "Short description for number three.",
            'due_date' => "27/08/2018",
            'created_by_id' => 1,
            'assigned_to_id' => 2,
        ]);
    }
}
