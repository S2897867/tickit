<?php

use Illuminate\Http\Request;
use App\Http\Middleware\jwtAuthCheck;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//
use App\User;
use App\Ticket;


/*
    the following routs are protected by autentication
*/
Route::group(['middleware' => ['api',]], function (){
    /* the following routs handle authentication */
   //log user in
    Route::post('/user/auth', 'Auth@login')->name('/user/auth');
    Route::post('/user/check', 'Auth@check')->name('/user/check');
    //create user
    Route::put('/user','apiController@createUser')->name('put->/user');
});

Route::group(['middleware' => ['api','jwtAuthCheck']], function () {
    Route::get('/test', function (){
        // // $user = new User();
        // // $ticket = new Ticket();
        // // return "test";
        // $tickets = User::find(2)->assignedTickets()->get();
        // $lodgeduser = $tickets[0]->lodgedBy();
        // dd($lodgeduser);
        // return;
        return view('test');
    })->name('test');



    //log user out
    Route::post('/user/logout',function (){
        return 'post -> /user/logout';
    });
    //get all users
    Route::post('/user/all','apiController@getAllUsers');

   //create ticket
    Route::put('/ticket','apiController@createTicket');

    //update ticket
    Route::patch('/ticket','apiController@updateTicket');

    //get all tickets
    Route::post('/tickets/{page}/{resolved}','apiController@getAllTickets');

    //get all tickets assigned to user
    Route::post('/tickets/my/{page}/{resolved}','apiController@getMyTickets');


});
?>
