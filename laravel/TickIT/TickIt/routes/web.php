<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use App\Ticket;

Route::get('/', function () {
    return view('welcome');
});
//
// Auth::routes();
//
// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', function (){
    // // $user = new User();
    // // $ticket = new Ticket();
    // // return "test";
    // $tickets = User::find(2)->assignedTickets()->get();
    // $lodgeduser = $tickets[0]->lodgedBy();
    // dd($lodgeduser);
    // return;
    return view('test');
})->name('test');
// 
// /*
//     the following routs handle authentication
// */
// //log user in
// Route::post('/user/auth', 'Auth@login')->name('/user/auth');
// Route::post('/user/check', 'Auth@check')->name('/user/check');
// //log user out
// Route::post('/user/logout','Auth@logout')->name('/user/logout');
//
// /*
//     the following routs are for creating uses
// */
// //create user
// Route::put('/user',function (){
//     return 'put -> /user';
// });
//
// /*
//     the following routs are protected by autentication
// */
// //create ticket
// Route::put('/ticket',function (){
// return 'put -> /tickets';
// });
//
// //update ticket
// Route::patch('/ticket',function (){
//     return 'post -> /ticket';
// });
//
// //get all tickets
// Route::post('/tickets',function (){
//     return 'post -> /tickets';
// });
//
// //get all tickets assigned to user
// Route::post('/tickets/my',function (){
//     return 'post -> /tickets/my';
// });
