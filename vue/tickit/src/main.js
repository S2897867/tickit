// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import Icon from 'vue-awesome/components/Icon'
//
// require("font-awesome-webpack");

Vue.config.productionTip = false

/*
temp date formmat function hack
 */


 Date.createFromMysql = function(mysql_string){
    var t, result = null;
    if( typeof mysql_string === 'string' ){
       t = mysql_string.split(/[- :]/);
       //when t[3], t[4] and t[5] are missing they defaults to zero
       result = new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
    }
    return result;
 }
 Date.toAUstring = function (date){
     let tempDate = Date.createFromMysql(date);
     return tempDate.toLocaleDateString("en-au", {year: "numeric", month: "numeric",day: "numeric"}).replace(/\s/g,'-')
 }
Date.dateObjToAUstring = function (tempDate){
  return tempDate.toLocaleDateString("en-au", {year: "numeric", month: "numeric",day: "numeric"}).replace(/\s/g,'-')
}



 Vue.filter('formatDate', function (mySQLDate) {
       if (mySQLDate) {
         let tempDate = Date.createFromMysql(mySQLDate);
         return tempDate.toLocaleDateString("en-au", {year: "numeric", month: "numeric",day: "numeric"}).replace(/\s/g,'-')
     }else{
         return "";
     }

 })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
      App,
      // Icon,
  },
  template: '<App/>',
});
// Vue.use(require('vue-moment'));
// Vue.filter('formatDate', function(mySQLDate) {
//   if (mySQLDate) {
//     // return moment(String(mySQLDate)).format('YYYY/MM/DD HH:mm:ss');
//     // let tempDate = Date.createFromMysql(mySQLDate);
//     // return tempDate.toLocaleDateString("en-au", {year: "numeric", month: "short",day: "numeric"}).replace(/\s/g,'-')
//     return "10";
//   }
// });
